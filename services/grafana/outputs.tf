output "public_ip" {
  description = "The public IP address of the web server"
  value       = module.stage_aws_instance.public_ip[0]
}
