#!/bin/bash -xe
# echo "Install Ansible ..."
# exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
cd /home/ubuntu/
sudo apt update -y
sudo apt install python3-pip -y
sudo apt install ansible -y

# echo "Install Docker ..."
git clone https://gitlab.com/adv-public/skillbox-diplom/infrastructure/ansible/install-docker.git
cd /home/ubuntu/install-docker
ansible-playbook playbooks/pl_install_docker_local.yml

# echo "Deploying application..."
cd /home/ubuntu/
git clone https://gitlab.com/adv-public/skillbox-diplom/infrastructure/ansible/setup-monitoring.git
cd /home/ubuntu/setup-monitoring/
ansible-playbook playbooks/pl_grafana_local.yml
