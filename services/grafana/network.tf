## Network
data "terraform_remote_state" "vpc-prod" {
  backend = "local"
  
  config = {
    path = "${path.module}/../../../../global/aws-vpc/state/terraform.tfstate"
     
   }
}

## Security group
resource "aws_security_group" "instance" {
  name        = "${local.environment}-${var.instance_security_group_name}"
  description = "Allow Public Access"
  vpc_id      = local.vpc_id
  
  # Let's set a rule on which ports we can access our servers
  egress = [ 
    {
      cidr_blocks      = [ "0.0.0.0/0" ]
      description      = "Allow all outbound requests"
      from_port        = 0
      protocol         = "-1"
      self             = false
      to_port          = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
    } 
  ]

  tags = merge(
    module.stage_aws_instance.tags,
    {
      "Name" = format("%s%s%s", local.name_instance, var.delimiter, local.environment)
      "AZ"   = local.availability_zone
    }
  )
}
resource "aws_security_group_rule" "instance_in_http" {
  description       = "Allow inbound HTTP requests"
  type              = "ingress"
  from_port         = var.public_in_port
  to_port           = var.public_in_port
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.instance.id
}

resource "aws_security_group_rule" "instance_in_ssh" {
  description       = "Allow inbound SSH requests"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.instance.id
}
