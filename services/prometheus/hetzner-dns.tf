# DNS-Record config
module "hetzner-dns" {
  source    = "git@gitlab.com:adv-public/skillbox-diplom/infrastructure/terraform/modules.git//terraform-hetzner-dns?ref=v0.0.1"

  dns_zone  = "denav.net"
  dns_name  = "monitoring"
  dns_value = module.stage_aws_instance.public_ip[0]
  dns_type  = "A"
  dns_ttl   = "3600"
}