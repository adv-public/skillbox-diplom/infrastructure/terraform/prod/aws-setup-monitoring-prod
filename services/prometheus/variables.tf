locals {
  environment       = "prod"
  name_instance     = "prometheus"

  # Network
  availability_zone = element(data.aws_availability_zones.available.names, 0)
  vpc_id            = data.terraform_remote_state.vpc-prod.outputs.vpc_id-prod
  subnet_id         = data.terraform_remote_state.vpc-prod.outputs.subnet_id-prod
}

#Module      : LABEL
#Description : Terraform label module variables.

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `organization`, `environment`, `name` and `attributes`."
}

#Module      : SUBNET
#Description : Terraform SUBNET module variables.

variable "availability_zones" {
  type        = list(string)
  default     = []
  description = "List of Availability Zones (e.g. `['us-east-1a', 'us-east-1b', 'us-east-1c']`)."
}

variable "public_in_port" {
  description   = "The public port the Prometheus will use for HTTP requests"
  type          = number
  default       = 80
}

variable "prometheus_in_port" {
  description   = "The internal port the Prometheus will use for HTTP requests"
  type          = number
  default       = 9090
}

variable "alertmanager_in_port" {
  description   = "The internal port the Alertmanager will use for HTTP requests"
  type          = number
  default       = 9093
}

variable "instance_security_group_name" {
  description   = "The name of the security group for the EC2 Instances"
  type          = string
  default       = "monitoring-instance"
}

variable "git_project_url" {
  description   = "The name of the security group for the EC2 Instances"
  type          = string
  default       = "https://gitlab.com/adv-public/skillbox-diplom/infrastructure/ansible/setup-monitoring.git"
}
