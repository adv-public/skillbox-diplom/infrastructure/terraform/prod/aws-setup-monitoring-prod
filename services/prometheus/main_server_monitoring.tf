terraform {
  required_version = ">= 1.0.0, < 2.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

## Configure the AWS Provider
provider "aws" {
  region = "eu-central-1"
}

# Find out what data centers are in the selected region
data "aws_availability_zones" "available" {}

## We are looking for an image with the latest version of Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
}

# Instance
module "stage_aws_instance" {
  # source
  # source     = "git@gitlab.com:adv-public/skillbox-diplom/infrastructure/terraform/modules.git//terraform-aws-ec2?ref=main"
  source = "../../../../../modules/terraform-aws-ec2"
  
  # labels
  name        = local.name_instance
  environment = local.environment
  label_order = ["name", "environment"]

  # instance
  instance_enabled = true
  instance_count   = 1
  ami              = data.aws_ami.ubuntu.id
  instance_type    = "t2.micro"
  monitoring       = false
  tenancy          = "default"
  hibernation      = false
  ebs_optimized    = false

  # Networking
  vpc_security_group_ids_list = [aws_security_group.instance.id]
  subnet_ids                  = tolist(local.subnet_id)
  assign_eip_address          = false
  associate_public_ip_address = true

  # Keypair
  key_name = "dd-web_denav"

  # User Data script as a template
  user_data = templatefile("user_data.sh", {
    git_project_url = var.git_project_url
  })

}